package mvc;


import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mvc.action.loginAction;
import mvc.action.regAction;
import mvc.form.Action;
import mvc.form.ActionForm;
import mvc.form.loginform;
import mvc.unti.FullBean;

public class ActionServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4088409260569148479L;
	

	public void doGet(HttpServletRequest request,HttpServletResponse response) throws
	IOException,ServletException
	{
		
		ActionForm form=FullBean.full(request);
		
		Action action=null;
		
	   Map<String,String> nowmap=ActionMap.getmap();
	   
	   try
	   {
		   String action_name=nowmap.get(request.getParameter("sign"));
		   
		   Class cla=Class.forName(action_name);
		   action=(Action) cla.newInstance();
		   
		   
	   }
	   catch(Exception e)
	   { 
		   
	   }
		
		String message=action.execute(form);
		
		PrintWriter out=response.getWriter();
		
		out.print(message);
		
		out.flush();
		out.close();
		
		//System.out.println(form);
		
	}
	
	

	public void doPost(HttpServletRequest request,HttpServletResponse response) throws
	IOException,ServletException
	
	{
		
		this.doGet(request, response);
		
	}
	
}
