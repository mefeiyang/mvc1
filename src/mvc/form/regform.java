package mvc.form;

public class regform extends ActionForm {


private String username;
private String password;
private String mobnum;
public String getUsername() {
	return username;
}
public void setUsername(String username) {
	this.username = username;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getMobnum() {
	return mobnum;
}
public void setMobnum(String mobnum) {
	this.mobnum = mobnum;
}
@Override
public String toString() {
	return "regform [username=" + username + ", password=" + password
			+ ", mobnum=" + mobnum + ", getUsername()=" + getUsername()
			+ ", getPassword()=" + getPassword() + ", getMobnum()="
			+ getMobnum() + ", getClass()=" + getClass() + ", hashCode()="
			+ hashCode() + ", toString()=" + super.toString() + "]";
}



}
