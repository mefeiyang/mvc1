package mvc.unti;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import mvc.form.ActionForm;

public class FullBean {
	
	
	public FullBean()
	{
		
	}
	
	
	public static ActionForm full(HttpServletRequest request)
	
	{
		
		ActionForm o=null;
		
		try
		{
			Class classnew=Class.forName(request.getParameter("sign"));
			o=(ActionForm) classnew.newInstance();
			
			Field[] filed=classnew.getDeclaredFields();
			
			for(Field f:filed)
			{
				
				f.setAccessible(true);
				f.set(o,request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		}
		catch(Exception e)
		
		{
			
			
		}
		
		return o;
	}
	

}
